import cherrypy
import sqlite3
import os
import json



def db_request(query):
    data_base = os.environ['db_name']
    con = sqlite3.connect(data_base)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute(query)
    con.commit()
    # result = []
    # while True:
    #     temp = cur.fetchone()
    #     if temp:
    #         result.append(dict(temp))
    #     else:
    #         break
    result = [dict(i) for i in cur.fetchall()]
    con.close()
    return result


class Router(object):
    def __init__(self):
        self.album = Album()
        self.song = Song()
        self.band = Band()
        self.search = Search()

    def _cp_dispatch(self, vpath):
        if len(vpath) == 0: # /
            return self.search
        elif len(vpath) == 1: # /search
            resource_type = vpath.pop(0)
            if resource_type == 'search':
                return self.search
            else:
                return self

        elif len(vpath) == 2: # /artist/<artist>
            vpath.pop(0)
            cherrypy.request.params['artist'] = vpath.pop(0)  # /artist name/
            return self.band

        elif len(vpath) == 4: # /artist/<artist>/album/<album> or /artist/<artist>/song/<song>
            vpath.pop(0)
            cherrypy.request.params['artist'] = vpath.pop(0)  # /artist name/
            resource_type = vpath.pop(0)
            if resource_type == 'album': # album name
                cherrypy.request.params['album'] = vpath.pop(0)
                return self.album
            elif resource_type == 'song': # song name
                cherrypy.request.params['song'] = vpath.pop(0)
                return self.song
            else:
                return self

        else:
            vpath = []
            return self


    @cherrypy.expose
    def index(self):
        return 'Wrong request'


class Album(object):
    @cherrypy.expose
    def index(self, artist, album):
        query = f"""SELECT
                        artist_table.song_id,
                        artist_table.track_num,
                        song.song_name,
                        song.song_year,
                        album.album_name,
                        album.album_info
                    FROM song JOIN artist_table on
                        song.song_id = artist_table.song_id
                    JOIN album on
                        artist_table.album_id = album.album_id
					JOIN artist on
						artist.id_artist = artist_table.artist_id
                    WHERE album.album_name = "{album}" and artist.artist_name = "{artist}"
                """
        result = db_request(query)
        return json.dumps(result)


class Song(object):
    @cherrypy.expose
    def index(self, artist, song):
        query = f"""SELECT
                       song.song_id,
					   artist.artist_name,
					   song.song_name,
					   song.song_year,
					   song_text
                    FROM song JOIN artist_table on
                        song.song_id = artist_table.song_id
                    JOIN artist on
						artist.id_artist = artist_table.artist_id					
                    WHERE song.song_name = "{song}" and 
                        artist.artist_name = "{artist}"
                    """
        result = db_request(query)
        return json.dumps(result)


class Band(object):
    @cherrypy.expose
    def index(self, artist):
        query = f""" SELECT DISTINCT
                       artist.artist_name,
                       artist.artist_info,
                       album.album_name,
                       album.album_id
                    FROM artist_table JOIN artist on
                        artist_table.artist_id = artist.id_artist
                    JOIN album on artist_table.album_id = album.album_id
                    WHERE artist.artist_name = "{artist}"
                    """
        result = db_request(query)
        return json.dumps(result)


class Search(object):
    @cherrypy.expose
    def index(self, search_string):
        result = {
            'artist': db_request(f"""SELECT 
                            id_artist, artist_name 
                        FROM artist 
                        WHERE artist_name like '%{search_string}%'"""),
            'album': db_request(f"""SELECT 
                            album_id, album_name 
                        FROM album 
                        WHERE album_name like '%{search_string}%'"""),
            'song': db_request(f"""SELECT
                            song_id, song_name
                        FROM song 
                        WHERE song_name like '%{search_string}%'""")
        }
        return json.dumps(result)


if __name__ == '__main__':
    cherrypy.quickstart(Router())
