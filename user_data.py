import sqlite3
import os


def db_request(query):
    data_base = os.environ['db_name']
    con = sqlite3.connect(data_base)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute(query)
    con.commit()
    result = [dict(i) for i in cur.fetchall()]
    con.close()
    return result


def search(search_string):
    search_dict = {}
    for table in ['artist', 'album', 'song']:
        search_dict.update({table: db_request(
            f'''SELECT * FROM {table} WHERE {table}_name like "%{search_string}%"'''
        )})
    return search_dict


def band(artist):
    query = f""" SELECT DISTINCT
                           artist.artist_name,
                           artist.artist_info,
                           album.album_name,
                           album.album_id
                        FROM artist_table JOIN artist on
                            artist_table.artist_id = artist.id_artist
                        JOIN album on artist_table.album_id = album.album_id
                        WHERE artist.artist_name = "{artist}"
                        """
    result = db_request(query)
    return result


def album(artist, album):
    query = f"""SELECT
                    artist_table.song_id,
                    artist_table.track_num,
                    song.song_name,
                    song.song_year,
                    album.album_name,
                    album.album_info
                FROM song JOIN artist_table on
                    song.song_id = artist_table.song_id
                JOIN album on
                    artist_table.album_id = album.album_id
                JOIN artist on
                    artist.id_artist = artist_table.artist_id
                WHERE album.album_name = "{album}" and artist.artist_name = "{artist}"
            """
    result = db_request(query)
    return result


def song(artist, song):
    query = f"""SELECT
                   song.song_id,
                   artist.artist_name,
                   song.song_name,
                   song.song_year,
                   song_text
                FROM song JOIN artist_table on
                    song.song_id = artist_table.song_id
                JOIN artist on
                    artist.id_artist = artist_table.artist_id					
                WHERE song.song_name = "{song}" and 
                    artist.artist_name = "{artist}"
                """
    result = db_request(query)
    return result
